
from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser
from datetime import datetime, timedelta
from operator import itemgetter


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = "AIzaSyCga0zkX-J0T695AfygE436AKkgtD316fk"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"


def youtube_search(options):
	youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
	developerKey=DEVELOPER_KEY)
	search_response = youtube.search().list(q=options.q,part="id",maxResults=options.max_results,publishedAfter=options.datetime,
		order="viewCount", type="video").execute()

	videos = []
	for search_result in search_response.get("items", []):
		videoId = search_result["id"]["videoId"]
		video_response = youtube.videos().list(id=videoId, part="statistics,snippet").execute()
		for video_result in video_response.get("items",[]):
			title = video_result["snippet"]["title"]
			viewCount = video_result["statistics"]["viewCount"]
			videos.append({"videoId": videoId, "title": title, "viewCount": int(viewCount)})
			break
	
	videos = sorted(videos, key=itemgetter("viewCount"), reverse=True)
	for video in videos[:10]:
		print video["videoId"],"\t", video["title"],"\t", video["viewCount"], "\n"


if __name__ == "__main__":
	now = datetime.now()
	d = (now - timedelta(days=1))
	one_day_ago= d.isoformat("T") + "Z"

	print "\n","\n", now, "\t",d, "\n"

	argparser.add_argument("--q", help="Search term", default="Google")
	argparser.add_argument("--max-results", help="Max results", default=50)
	argparser.add_argument("--datetime", help="start date, in YYYY-MM-DD format",default=one_day_ago)

	args = argparser.parse_args()

	try:
		youtube_search(args)
	except HttpError, e:
		print "An HTTP error %d occurred:\n%s" % (e.resp.status, e.content)
